preco = float(input("Informe o valor do Produto:"))
desconto = float(input("Informe o percentual de Desconto:"))
valor_desconto = preco * desconto / 100
preco_final = preco - valor_desconto
print("O valor do Desconto foi de: R$ %5.2f" % valor_desconto)
print("O Preço a pagar é de: R$ %5.2f" % preco_final)

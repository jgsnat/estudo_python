salario_base = float(input("Informe o Salário Base:"))
porcentagem_aumento = float(input("Informe a Porcentagem de Aumento:"))
aumento = salario_base * porcentagem_aumento / 100
salario_reajustado = salario_base + aumento
print("Seu aumento foi de R$ %5.2f" % aumento)
print("Seu Salário com Aumento ficou: R$ %5.2f" % salario_reajustado)
